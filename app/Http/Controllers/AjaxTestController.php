<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class AjaxTestController extends Controller
{
    public function index()
    {
    	return view('ajaxtest');
    }

    public function show()
    {
    	return DB::table('products')
    		->where('status',1)
    		->orderBy('id',"DESC")
    		->get();
    }

    public function delete($id = null)
    {
    	$status = DB::table('products')
    			  ->where('id',$id)
    			  ->update(['status'=>0]);
    		if($status){
    			return ['status'=>true];
    		}else{
    			return ['status'=>false];
    		}
    }

    public function edit($id)
    {
        $product =  DB::table('products')
            ->where('id',$id)
            ->first();
        if (count($product)) {
            return response()->json(['product' => $product]);
        }else{
            return response()->json(['product' => false]);
        }
    }

    public function update(Request $request)
    {
        try {
            DB::table('products')
            ->where('id',$request->product_id)
            ->update([
                'name'       =>  $request->name,
                'price'      =>  $request->price,
                'quantity'   =>  $request->quantity,
                'updated_at' =>  date('Y-m-d h:i:s')
            ]);
        return response()->json(['success' => true,'status'=>'Product Update Successfull..!']);
        } catch (\Exception $e) {
           return response()->json(['success' => false,'status' => 'Fail to Update..!']); 
        }
    }



    
}

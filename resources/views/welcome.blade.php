<!doctype html>
<html lang="{{ app()->getLocale() }}">
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel-Ajax</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>

    <body>
        <div class="container" style="margin-top: 100px;">
            <div class="col-md-4">

                <div class="panel panel-default">
                    <div class="panel-body">
                        <form action="" method="POST" id="product-form">
                          <div class="form-group">
                            <label for="name">Product Name</label>
                            <input type="hidden" class="form-control" name="_token" id="token" value="{{csrf_token()}}">                            
                            <input type="text" class="form-control" name="name" id="name">                            
                          </div>
                          <div class="form-group">
                            <label for="price">Product Price</label>
                            <input type="text" class="form-control" name="price" id="price">                            
                          </div> 
                          <div class="form-group">
                            <label for="quantity">Product Quantity</label>
                            <input type="text" class="form-control" name="quantity" id="quantity">                            
                          </div>                          
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <button class="btn btn-warning" id="show-data">Load Product</button>                        
                    </div>

                    <div class="panel-body">
                        <img src="{{url('loading.gif')}}" height="30px" width="30px" style="display:none" id="loading">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody id="loadTableData">
                                {{-- Data Show Here --}}
                            </tbody>
                        </table>
                    </div>
                </div>            
            </div>
        </div>

       <div class="row">            
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Product</h4>
                  </div>
                  <div class="modal-body">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form action="" method="POST" id="edit-product-form">
                              <div class="form-group">
                                <label for="name">Product Name</label>
                                <input type="hidden" class="form-control" name="_token" id="edit-token" value="{{csrf_token()}}">                            
                                <input type="hidden" class="form-control" name="product_id" id="edit-id" value="">                            
                                <input type="text" class="form-control" name="name" id="edit-name">                            
                              </div>
                              <div class="form-group">
                                <label for="price">Product Price</label>
                                <input type="text" class="form-control" name="price" id="edit-price">                            
                              </div> 
                              <div class="form-group">
                                <label for="quantity">Product Quantity</label>
                                <input type="text" class="form-control" name="quantity" id="edit-quantity">
                              </div>                          
                              <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
       </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            //@@@===== Set Dom Object ======@@@
            var loadTableData = $('#loadTableData');
            var loading = $('#loading');

            $(document).ready(function() {
                loadData();
            });

            //@@@===== Insert Form Data Process ======@@@
            $('#product-form').on('submit',function(e){
                e.preventDefault();
                var data = $(this).serializeArray();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('store')}}",
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                    beforeSend: function(){
                        loading.show();
                    },
                    success: function(response){
                        if(response.success == true){
                            loadData();
                        }else if(response.success == false){
                            alert(response.status);
                        }
                        loading.hide();
                    }
                });
            });

            //@@@===== Edit Data process ======@@@
            $(document).on("click",".btn-edit",function() {
                var self = $(this);
                var id = self.attr('id').split('_')[1];
                $.ajax({
                    url: "{{url('edit')}}/"+id,
                    type: 'GET',
                    dataType: 'JSON',
                    beforeSend: function(){
                        loading.show();
                    },
                    success: function(responseData){
                        if(responseData.product != false){
                            $('#edit-id').val(responseData.product.id);
                            $('#edit-name').val(responseData.product.name);
                            $('#edit-price').val(responseData.product.price);
                            $('#edit-quantity').val(responseData.product.quantity);
                        }
                        loading.hide();
                    }
                });
            });

            //@@@===== Update Form Data Process ======@@@
            $('#edit-product-form').on('submit',function(e){
                e.preventDefault();
                var data = $(this).serializeArray();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('update')}}",
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                    beforeSend: function(){
                        loading.show();
                    },
                    success: function(response){
                        if(response.success == true){
                            loadData();
                             $('.modal').modal('toggle');
                        }else if(response.success == false){
                            alert(response.status);
                        }
                        loading.hide();
                    }
                });
            });

            //@@@===== Show Data Process ======@@@
            $('#show-data').on('click',function(){
                loadData();
            });

            //@@@===== Load Data Independed Function ======@@@
            function loadData(){
                $.ajax({
                    url: "{{url('show')}}",
                    type: 'GET',
                    dataType: 'JSON',
                    beforeSend: function(){
                        loading.show();
                        loadTableData.empty();
                    },
                    success: function(responseData){
                        var content = '';
                        $.each(responseData,function(key,val){
                            content += '<tr>'+
                                '<td>'+(++key)+'</td>'+
                                '<td>'+val.name+'</td>'+
                                '<td>'+val.price+'</td>'+
                                '<td>'+val.quantity+'</td>'+
                                '<td>'+
                                    '<button class="btn btn-primary btn-sm btn-edit" id="edit_'+val.id+'" data-toggle="modal" data-target="#myModal">Edit</button> '+
                                    '<button class="btn btn-danger btn-sm btn-delete" id="delete_'+val.id+'">Delete</button>'+
                                '</td>'+
                            '</tr>';
                        });
                        loadTableData.html(content);
                        loading.hide();
                    }
                });
            }

            //@@@===== Product Delete Process ======@@@
            $(document).on("click",".btn-delete",function() {
                var self = $(this);
                var id = self.attr('id').split('_')[1];
                $.ajax({
                    url: "{{url('delete')}}/"+id,
                    type: 'GET',
                    dataType: 'JSON',
                    beforeSend: function(){
                        loading.show();
                    },
                    success: function(responseData){
                        if(responseData.status === true){
                            self.parent().parent().remove();
                        }else if(responseData.status === false){
                            alert("Something is Wrong");
                        }
                        loading.hide();
                    }
                });
            });
        </script>

    </body>
</html>
